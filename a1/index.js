/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username alconslready exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
function registerNewUser(newUser) {
    let checkReg = true;

    // This portion of the function check whether the user is already in the registeredUsers array
    for (let i = 0; i <= registeredUsers.length; i++) {
        if (newUser == registeredUsers[i]) {
            alert("The user is already registered.");
            checkReg = false;
            break;
        } else {
            continue;
        }
    }

    // This portion of the function will determine if the inputted user will be added to the registeredUsers array.
    if (checkReg == true) {
        alert("User is now registered.")
        registeredUsers.push(newUser);
    }
}


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addToFriendsList(oldUser) {
    let checkReg = false;

    // This portion of the function check whether the user is already in the registeredUsers array
    for (let i = 0; i <= registeredUsers.length; i++) {
        if (oldUser == registeredUsers[i]) {
            checkReg = true;
            break;
        } else {
            continue;
        }
    }

    // This portion of the function will determine if the inputted user will be added to the friendsList array.

    if (checkReg == true) {
        alert("You have added " + oldUser + " as a friend.");
        friendsList.push(oldUser);
    } else {
        alert("User not found.");
    }
}

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function showFriendsList() {
    if (friendsList.length == 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        for (let i = 0; i < friendsList.length; i++) {
            console.log(friendsList[i]);
        }
    }
}

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function friendListCounter() {
    if (friendsList.length == 0) {
        alert("You currently have 0 friends. Add one first.")
    } else {
        alert("You currently have " + friendsList.length + " friends.");
    }
}

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function deleteUser(specificUser) {
    if (friendsList.length == 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        // If no argument is provided for the function, then remove the last element of the friendsList array
        if (specificUser == null) {
            friendsList.pop();
        // STRETCH GOAL: If an argument, which will take a form of a number/integer, is provided for the function, then delete the mentioned value of the index
        } else {
            friendsList.splice(specificUser, 1);
        }
    }
}

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/





