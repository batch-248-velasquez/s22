console.log("JavaScript - Array Manipulation");

//Array Methods
//JS has built-in functions and methods for arrays
//This allows us to manipulate and access array items

//Mutator Methods
/*
	Mutator Methods are functions that "mutate" or change an array after they are created
	These methods manipulate the original array performing various tasks such as adding and removing elements

*/

let fruits = ['Apple',"Orange", 'Kiwi', 'Dragon Fruit'];

//push
	/*
		adds an element in the end of an array and returns the array length

		Syntax:

		arrayName.push();

	*/
	
	console.log("Current Array: ");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength);
	console.log("Mutated array from push method: ");
	console.log(fruits);//Mango is added at the end of the array

	fruits.push('Avocado', 'Guava');
	console.log("Mutated array from push method: ");
	console.log(fruits);// Avocado and Guava is added at the end of the array
	//['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava']

//pop

/*
	Removes the last element in an array and returns the removed element
	Syntax:
	
	arrayName.pop();

*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);//Guava
	console.log("Mutated array from pop method: ");
	console.log(fruits);// Guava is removed
	//['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']




	let ghostFighters = ["Eugene","Dennis","Alfred","Taguro"];

	/*
		Mini Activity 1 (3min)
		Create a function which will remove the last person in the array
		log the ghostFighters array in the console
		Send an ss of your output

	*/

	// function removePerson(){

	// 	ghostFighters.pop();
	// }

	// removePerson();
	// console.log(ghostFighters);//['Eugene', 'Dennis', 'Alfred']

	//unshift()

	/*
		adds one or more elements at the beginning of an array

		syntax
			arrayName.unshift(elementA);
			arrayName.unshift(elementA, elementB);
	*/

		fruits.unshift('Lime', 'Banana');
		console.log("Mutated array from unshift method: ");
		console.log(fruits);//['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']


	//shift()

	/*
		removes an element at the beginning of an array AND returns the removed element

		syntax
			arrayName.shift();

	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method: ");
	console.log(fruits);//['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	//splice
	/*
		simultaneously removes elements from a specified index number and adds the elements

		Syntax
		arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);

	*/

	fruits.splice(1,2,'Lime','Cherry');
	console.log("Mutated array from splice method: ");
	console.log(fruits);//['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	//sort
	 /*
		Rearranges the array elements in alphanumeric order
		-Syntax
			arrayName.sort();

	 */

	fruits.sort();
	console.log("Mutated array from sort method");
	console.log(fruits);

	//reverse
	 /*
		Reverses the order of array elements
		-Syntax
			arrayName.reverse();
	 */

	fruits.reverse();
	console.log("Mutated array from reverse method");
	console.log(fruits);


	//Non-Mutator Methods
	/*
		Non-Mutator methods are functions that do not modify or change an array after they are created
		-these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

	let countries = ['US',"PH","CAN",'SG',"TH","PH","FR",'DE'];

	//indexOf()

	/*
		returns the index number of the first matching element found in an array
		--if no match is found, the result is -1
		-the search process will be done from first element processing to the last element

		syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue,fromIndex)

	*/

		let firstIndex = countries.indexOf('PH');
		console.log("Result of indexOf method: " + firstIndex);//1
		let firstExample = countries.indexOf('ph');
		console.log("Result of indexOf method: " + firstExample);//-1
		let invalidCountry = countries.indexOf('BR');
		console.log("Result of indexOf method: " + invalidCountry);//-1


	//lastIndexOf()

		/*
			returns the index number of the last matching element found in an array
			-the search process will be done from last element processing to the first element

			syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue,fromIndex)

		*/


		//['US',"PH","CAN",'SG',"TH","PH","FR",'DE'];

		let lastIndex = countries.lastIndexOf('PH', 3);
		console.log("Result of lastIndexOf method: " + lastIndex);//1


	//slice()

		/*
			-portions/slices elements from an array AND returns a new array
			-syntax
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex,endingIndex);
		*/



		console.log(countries);//['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

		let slicedArrayA = countries.slice(2);
		console.log('Result from slice method: ');
		console.log(slicedArrayA);//['CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
		
		console.log(countries);

		let slicedArrayB = countries.slice(2,4);
		console.log('Result from slice method: ');
		console.log(slicedArrayB);//'CAN', 'SG' //[02,03]

		let slicedArrayC = countries.slice(-3);
		console.log('Result from slice method: ');
		console.log(slicedArrayC);//['PH', 'FR', 'DE']


	//toString()

		/*
			retruns an array as a string separated by commas
			Syntax:
			arrayName.toString();

		*/

		let stringArray = ghostFighters.toString();
		console.log("Result from toString method: ");
		console.log(stringArray);//Eugene,Dennis,Alfred
		console.log(typeof stringArray);//string

	//concat()
		/*
			combine two arrays and returns the combined result
			-Syntax
				arrayA.concat(arrayB);
				arrayA.concat(elementA);
		*/


		let taskArrayA = ['drink html', 'eat JavaScript'];
		let taskArrayB = ['inhale css', 'breathe react'];
		let taskArrayC = ['get git', 'be node'];

		let tasks = taskArrayA.concat(taskArrayB);
		console.log('Result from concat method: ');
		console.log(tasks);//['drink html', 'eat JavaScript', 'inhale css', 'breathe react']

		//combine multiple arrays

		console.log("Result from concat method: ");
		let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
		console.log(allTasks);//['drink html', 'eat JavaScript', 'inhale css', 'breathe react', 'get git', 'be node']

		//combine arrays with elements
		let combinedTasks = taskArrayA.concat('smell express', 'throw mongodb');
		console.log("Result from concat method: ");
		console.log(combinedTasks);//['drink html', 'eat JavaScript', 'smell express', 'throw mongodb']

	//join
		/*
			Returns an array as a string separated by specified separator
			-syntax

			arrayName.join('separatorString');
		*/

		let users = ["John","Jane","Joe","Robert","Neil"];

		console.log(users.join());//John,Jane,Joe,Robert,Neil
		console.log(users.join(' '));
		console.log(users.join(' - '));
		console.log(users.join(' * '));//John * Jane * Joe * Robert * Neil

	//Iteration Method

	/*
		Iteration methods are loops designed to perform repetitive tasks on arrays
		Iteration methods loops over all items in an array
		Useful for manipulating array data resulting in complex tasks
	*/

//forEach
	/*
		similar to for loop that iterates on each array element
		-for each item in the array, the anonymous function passed in the forEach() method will be run
		-anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
		-variable names for arrays are normally written in the plural form of the data stored in an array
		-it's common practice to use the singular form of the array content for parameter names used in array loops
		-forEach() does NOT return anything


		allTasks.forEach (function(task)){
	
		}


	*/

		allTasks.forEach (function(task){
			console.log(task);
		})


	/*
		Mini Activity #2
		create a function that can display the ghostFighters one by one in our console
		Invoke the function
		send a screenshot
	*/

	/*
		Mini Activity #3
		Create an addTrainer function that will enable us to add a trainer in the contacts array
		--This function should be able to receive a string
		--Determine if the added trainer already exists in the contacts array:
		--if it is, show an alert saying "Already added in the Match Call(Contacts)"
		--if it is not, add the trainer in the contacts array and show an alert saying "Registered!"
		--invoke and add a trainer in the browser's console
		--in the console, log the contacts array
	*/

		
		function displayGhostFighters(){}
			ghostFighters.forEach (function(fighter){
			console.log(fighter);
		});

		displayGhostFighters()


		let filteredTasks =[]
		allTasks.forEach(function (task) {
			console.log(task)
			if(task.length>10){
				console.log (task + "my length is >10");
				filteredTasks.push (task)

			}
		})

		console.log("results of filteredTasks: ");
		console.log(filteredTasks)


		let numbers = [1, 2, 3, 4, 5];
		let numberMap = numbers.map (function(number){

			return number * number;
		})

		console.log ("Original Array: ");
		console.log(numbers);
		console.log("results of map method: ")
		console.log(numberMap)

		let numberForEach = numbers.forEach (function(number){

			return number * number;
		})

		console.log(numberForEach);


		//every()


		let allValid = numbers.every(function(number){
			return (number <3)
		});
		console.log(allValid);

		//some()
		let someValid = numbers.some(function(number){
			return (number <2)
		});
		console.log(someValid);

		//filter()
		let filterValid = numbers.filter(function(number) {
			return(number <3)

		})

		console.log(filterValid)

		let nothingFound = numbers.filter(function(number) {
				return (number =0)

		})
		console.log(nothingFound)


		let filteredNumbers = []
		numbers.forEach(function(number){
			if (number<3){
				filteredNumbers.push(number)
			}
		})

		console.log(filteredNumbers)
